const { findTodo, deleteByID, updateById, insert, clearTodo, todos } = require('./todo');
describe('Test todo model', () => {
  it('should create todo & return inserted todo', () => {
    const inserted = insert({ id: 1, title: 'First Todo' });
    expect.assertions(3);
    expect(inserted).toEqual({
      id: 1,
      title: 'First Todo',
      completed: false,
    });
    expect(todos.length).toEqual(1);
    expect(todos[0]).toEqual({
      id: 1,
      title: 'First Todo',
      completed: false,
    });
  });
  it('should update todo & return updated one', () => {
    const todoUpdate = { id: 1, title: 'First todo updated', completed: true };
    expect.assertions(2);
    const updated = updateById(todoUpdate);
    expect(todos[0]).toEqual({ id: 1, title: 'First todo updated', completed: true });
    expect(todos.length).toEqual(1);
  });

  it('should delete todo & return deleted one', () => {
    const todoID = { id: 1 };
    expect.assertions(1);
    const deleted = deleteByID(todoID);
    expect(todos.length).toEqual(0);
  });

  it('should find all todo & return list with state completed', () => {
    const listTodoArr = [
      { id: 2, title: 'Second Todo' },
      { id: 3, title: 'Third Todo' },
      { id: 4, title: 'fourth Todo' },
    ];
    listTodoArr.forEach(t => insert(t));
    updateById({ id: 3, title: 'Third Todo', completed: true });
    const completed = true;
    const listTodo = findTodo({ completed });
    console.log(listTodo);
    expect.assertions(1);
    expect(listTodo.length).toEqual(1);
  });
  it('should clear all todo & return todos empty', () => {
    const todos = clearTodo();
    expect.assertions(1);
    expect(todos.length).toEqual(0);
  });
});
