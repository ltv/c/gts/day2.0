let todos = [];
/**
 * create new todo
 * @param {todo} todo
 * @return {todo}
 */
function insert(todo) {
  const indexTodo = todos.findIndex(t => t.id == todo.id);
  if (indexTodo === -1) {
    const item = {
      ...todo,
      completed: false,
    };
    todos.push(item);
    return item;
  } else {
    return todos[indexTodo];
  }
}
/**
 * update new todo by id
 * @param {todo} todo
 * @return {todo} success
 * @return {false} error
 */
function updateById(todo) {
  const todoIndex = todos.findIndex(t => t.id == todo.id);
  if (todoIndex != -1) {
    todos[todoIndex] = { ...todos[todoIndex], ...todo };
    return todos[todoIndex];
  }
  return false;
}
/**
 * remove todo by id
 * @param {id:  number}
 * @return {boolean}
 */
function deleteByID(todo) {
  const todoIndex = todos.findIndex(t => t.id == todo.id);
  if (todoIndex != -1) {
    todos.splice(todoIndex, 1);
    return true;
  }
  return false;
}
/**
 * find all todo with completed true or false
 * @param {completed: boolean} completed
 */
function findTodo(todo) {
  if (todo.completed != undefined) {
    return todos.filter(t => t.completed == todo.completed);
  }
  return todos;
}
/**
 * clear all todo
 */
function clearTodo() {
  todos = [];
  return todos;
}
module.exports = {
  findTodo: findTodo,
  deleteByID: deleteByID,
  updateById: updateById,
  insert: insert,
  clearTodo: clearTodo,
  todos: todos,
};
