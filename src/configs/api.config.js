const api = '/api/';
const API = {
  CREATE: api + 'createTodo',
  UPDATE: api + 'updateTodo',
  GET_ALL: api + 'todos',
  GET_ALL_COMPLETED: api + 'todos',
  GET_ALL_ACTIVETED: api + 'todos',
  DELETE_TODO: api + 'deleteTodo',
  CLEAR_COMPLETED: api + 'clearCompleted',
};

module.exports = API;
