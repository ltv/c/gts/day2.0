const todoModel = require('../models/todo');

function createTodo(req, res, next) {
  res.json(todoModel.insert(req.body));
}
function updateTodo(req, res, next) {
  res.json(todoModel.updateById(req.body));
}
function getTodos(req, res, next) {
  res.json(todoModel.findTodo(req.body));
}

function deleteTodo(req, res, next) {
  res.json(todoModel.deleteByID(req.body));
}
function clearTodo(req, res, next) {
  res.json(todoModel.clearTodo(req.body));
}

module.exports = {
  createTodo: createTodo,
  updateTodo: updateTodo,
  getTodos: getTodos,
  deleteTodo: deleteTodo,
  clearTodo: clearTodo,
};
