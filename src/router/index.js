const express = require('express');
const router = express.Router();
const API = require('../configs/api.config');

const { createTodo, updateTodo, getTodos, deleteTodo, clearTodo } = require('../controller/todocontroller');
router.post(API.CREATE, createTodo);

router.post(API.UPDATE, updateTodo);

router.get(API.GET_ALL_ACTIVETED, getTodos);

router.get(API.GET_ALL_COMPLETED, getTodos);

router.get(API.CLEAR_COMPLETED, clearTodo);

router.post(API.DELETE_TODO, deleteTodo);

module.exports = router;
