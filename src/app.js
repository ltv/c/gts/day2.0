const express = require('express');
const app = express();
const router = require('./router/index');
const bodyParser = require('body-parser');
const all_routes = require('express-list-endpoints');
const cors = require('cors');

const PORT = 3000;

// body-parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// cors
app.use(cors());
app.use('', router);
console.log(all_routes(app));

app.listen(PORT, () => {
  console.log(`Server running PORT: ${PORT}`);
});
